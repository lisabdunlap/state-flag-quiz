cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/com.sessionm.phonegap/www/sessionm.js",
        "id": "com.sessionm.phonegap.sessionm",
        "clobbers": [
            "sessionm.phonegap"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "org.apache.cordova.device": "0.2.13",
    "com.sessionm.phonegap": "0.1.2"
}
// BOTTOM OF METADATA
});